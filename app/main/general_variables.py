from dotenv import load_dotenv 
import os
from pathlib import Path

env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path, override=True)

API_KEY =os.getenv("API_KEY")
URL =os.getenv("URL")
