import aiohttp
from aiohttp import ClientSession
import asyncio
import datetime
import json
import general_variables

#!CONECCION CON EL WEBSOCKET DESDE RPI HACIA EL SERVER
async def ejecutar_ws(session: ClientSession, ruta_ws: str):

    async with session.ws_connect(url=ruta_ws) as ws:
        test_msg: dict = {"action": "SERVER_TEST", "msg": "nuevo_mensaje"}

        await ws.send_str(json.dumps(test_msg))
        async for msg in ws:
            print("msg recieved", msg)

            if msg.type in (aiohttp.WSMsgType.CLOSED, aiohttp.WSMsgType.ERROR):
                print("Termino WS")
                break


async def run(url: str, token: str):
    auth_cookie = {"cookie_jwt": token}
    ruta_ws = f"{url}/api/v1.0/ws/device"
    async with aiohttp.ClientSession(cookies=auth_cookie) as session:
        web_socket = await ejecutar_ws(session=session, ruta_ws=ruta_ws)


if __name__ == "__main__":

    url: str = general_variables.URL
    token: str = general_variables.API_KEY
    #!LEVANTA EL ASYNCIO
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(url=url, token=token))
    print("termine")
